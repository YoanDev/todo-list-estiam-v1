# Qui a fait quoi ?

### Yoan MENDES SEMEDO : 
- création todo list 
- animation svg 
- animation framer motion 
- animation aos 
- css 
- test unitaire

### Lory Stan TANASI-CHAMSOUDINE : 
- ajout d'animations lors de l'ajout d'un todo et lors de la suppression d'un todo, à l'aide de la librairie mui.

### Amine ABBES : 
- animation pure CSS et framework (material UI)


### Alexandre BENALI : 
- animation pure CSS et framework (material UI)