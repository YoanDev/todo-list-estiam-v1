import './App.css';
import './styles.css';
import TodoList from './components/TodoList';
import IllustrationThreeSVG from './SVG/IllustrationThreeSvg';
import IllustrationTodoSVG from './SVG/IllustrationTodoSvg';
import Date from '../src/components/Date';

function App() {
  return (
    <div className='App'>
      <IllustrationThreeSVG className="template1" />
      <IllustrationTodoSVG className="template2" />
      <TodoList />
      <Date/>
    </div>
  );
}

export default App;
