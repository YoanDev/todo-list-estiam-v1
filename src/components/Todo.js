import React, { useEffect, useState } from "react";
import "./todo.css";
import { FiDelete } from "react-icons/fi";
import { MdModeEdit } from "react-icons/md";
import Aos from "aos";
import "aos/dist/aos.css";
import { buttonVariants } from "../animations";
import { motion } from "framer-motion";
import { handleClick } from "./utils/todoMethods";

function Todo({ todo, remove, update, toggleComplete }) {
  const [isEditing, setIsEditing] = useState(false);
  const [task, setTask] = useState(todo.task);

  useEffect(() => {
    Aos.init();
  }, []);

  const toggleFrom = () => {
    setIsEditing(!isEditing);
  };
  const handleUpdate = (evt) => {
    evt.preventDefault();
    update(todo.id, task);
    toggleFrom();
  };
  const handleChange = (evt) => {
    setTask(evt.target.value);
  };
  const toggleCompleted = (evt) => {
    toggleComplete(evt.target.id);
  };

  let result;
  if (isEditing) {
    result = (
      <div className="Todo">
        <form className="Todo-edit-form" onSubmit={handleUpdate}>
          <input onChange={handleChange} value={task} type="text" />
          <motion.button variants={buttonVariants} whileHover="hover">
            Save
          </motion.button>
        </form>
      </div>
    );
  } else {
    result = (
      <div className="Todo" data-aos="zoom-in">
        <li
          id={todo.id}
          onClick={toggleCompleted}
          className={todo.completed ? "Todo-task completed" : "Todo-task"}
        >
          {todo.task}
        </li>
        <div className="Todo-buttons">
          <button onClick={toggleFrom} className="test-btn">
            <MdModeEdit />
          </button>
          <button
            className="test-btn"
            onClick={() => handleClick(todo, remove)}
          >
            <FiDelete />
          </button>
        </div>
      </div>
    );
  }
  return result;
}

export default Todo;
