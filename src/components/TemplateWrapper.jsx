import React from 'react'
import './templateWrapper.css'

const TemplateWrapper = ({children}) => {
  return (
    <div className='template-wrapper'>
        {children}
    </div>
  )
}

export default TemplateWrapper