import React, { useEffect, useState } from "react";
import Todo from "./Todo";
import NewTodoForm from "./NewTodoForm";
import { v4 as uuid } from 'uuid';
import "./todoList.css";
import Aos from "aos";
import 'aos/dist/aos.css'
import TodoListSVG from "../SVG/TodoListSvg";
import Collapse from '@mui/material/Collapse';
import List from '@mui/material/List';
import { TransitionGroup } from 'react-transition-group';


function TodoList() {
  const [todos, setTodos] = useState([
    { id: uuid(), task: "task 1", completed: false },
    { id: uuid(), task: "task 2", completed: true }
  ]);

  useEffect(() => {
    Aos.init();
  }, [])


  const create = newTodo => {
    console.log(newTodo);
    setTodos([...todos, newTodo]);
  };

  const remove = id => {
    setTodos(todos.filter(todo => todo.id !== id));
  };

  const update = (id, updtedTask) => {
    const updatedTodos = todos.map(todo => {
      if (todo.id === id) {
        return { ...todo, task: updtedTask };
      }
      return todo;
    });
    setTodos(updatedTodos);
  };

  const toggleComplete = id => {
    const updatedTodos = todos.map(todo => {
      if (todo.id === id) {
        return { ...todo, completed: !todo.completed };
      }
      return todo;
    });
    setTodos(updatedTodos);
  };

  return (
    <div data-aos="fade-out">
      <div className="TodoList">
        <TodoListSVG className="todolist-title" />
        <List>
          <TransitionGroup>
            {todos.map((todo) => (
              <Collapse>
                <Todo
                  toggleComplete={toggleComplete}
                  update={update}
                  remove={remove}
                  key={todo.id}
                  todo={todo}
                />
              </Collapse>
            ))}
          </TransitionGroup>
        </List>
        <NewTodoForm createTodo={create} />
      </div>
    </div>
  );
}

export default TodoList;
