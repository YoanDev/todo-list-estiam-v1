import { fireEvent, render } from "@testing-library/react";
import Todo from "./Todo";
import { handleClick } from "./utils/todoMethods";

describe("Test todo", () => {
    beforeAll(() => {
        jest.clearAllMocks();
    });
  it("Todo is defined", () => {
    render(
      <Todo
        toggleComplete={()=>{}}
        update={()=>{}}
        remove={()=>{}}
        key={()=>{}}
        todo={()=>{}}
      />
    );
    const container = document.getElementsByClassName("Todo");
    expect(container).toBeDefined();
  });
  it("handleClick is called", () => {
    render(
      <Todo
        toggleComplete={()=>{}}
        update={()=>{}}
        remove={()=>{}}
        key={()=>{}}
        todo={()=>{}}
      />
    );
    const btn = document.getElementsByClassName('test-btn')
    // fireEvent.click(btn)
    expect(btn).toBeDefined();
    // expect(handleClick).toHaveBeenCalledTimes(1);

    //ceci ne fonctionne pas et ça m'énerve

  });
});
